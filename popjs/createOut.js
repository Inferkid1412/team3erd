//Written by: Samuel Hill

function outNameFunc(){
	fName = [
		"Liam",
		"Noah",
		"William",
		"James",
		"Oliver",
		"Benjamin",
		"Elijah",
		"Lucas",
		"Mason",
		"Logan",
		"Emma",
		"Olivia",
		"Avaa",
		"Isabella",
		"Sophia",
		"Charlotte",
		"Mia",
		"Amelia",
		"Harper",
		"Evelyn",
		"Sam",
		"Moon"
	];

	lName = [
		"Smith",
		"Johnson",
		"Williams",
		"Jones",
		"Brown",
		"Davis",
		"Miller",
		"Wilson",
		"Moore",
		"Taylor",
		"Anderson",
		"Thomas",
		"Jackson",
		"White",
		"Harris",
		"Martin",
		"Thompson",
		"Garcia",
		"Martinez",
		"Robinson",
		"Hill",
		"Moon"
	];
	
	outString = "";
	USCcount = 1000;

	while (USCcount <= 1400){
		randNumF = Math.floor(Math.random()*22);
		randNumL = Math.floor(Math.random()*22);

		//only 5 majors
		randMajorId = Math.floor(Math.random()*5)+1;

		outString = outString + (
			"(" + USCcount + ", '" +
			fName[randNumF] + "', '" +
			lName[randNumL] + "', '" +
			fName[randNumF] + USCcount + "@uscb.edu', " +
			randMajorId +
			"),<br>"
		);

		USCcount++;
	};
	document.getElementById("outName").innerHTML = (
		outString	
		
	);
}

//this creates a string of giberish
function gibGen(){
	var randWords = [
		"cat ",
		"art ",
		"human ",
		"computer ",
		"terminator ",
		"and ",
		"expert ",
		"study "
	];

	gibOut = "'";
	var lengthWords = Math.floor(Math.random()*5)+2;

	count = 0;
	while (count < lengthWords){
		var randWord = Math.floor(Math.random()*8);

		gibOut = gibOut + randWords[randWord];

		count++;
	};
	
	gibOut = gibOut + "'";
	return gibOut;

}

function outGroupFunc(){
	outString = "";
	groupId = 1;

	while (groupId <= 100){
		
		//50 mentors of which Ids start at 1
		var randMentorId = Math.floor(Math.random()*50) + 1;

		//5 catagories of which Ids start at 1
		var randCatId = Math.floor(Math.random()*5) + 1;

		//NOT NEEDED
		//401 students of which Ids start at 1000 and end at 1400
		//var randLStud = Math.floor(Math.random()*400) + 1000;

		//100 projects of which Ids start at 1 and end at 100

		if (groupId <= 25){
			year = 2016;
		} else if (groupId <= 50){
			year = 2017;
		} else if (groupId <= 75){
			year = 2018;
		} else {
			year = 2019;
		}

		outString = outString + (
			"(" + groupId + ", " +
			year + ", " +
			randMentorId + ", " +
			randCatId + ", " +
			leaderArray[groupId] + ", " +
			groupId + "),<br>"

		);
		groupId++;
	};

	document.getElementById("outGroup").innerHTML = (
		outString	
	);
}

function outGroupStudentLinkFunc(){
	outString = "";
	leaderArray = []; //the reason why this must run first

	groupCounter = 1; 
	while (groupCounter <= 100){
		var numStudInGroup = Math.floor(Math.random()*5) + 1;
		
		counter = 1;
		while (counter <= numStudInGroup){
			//random student
			var randStud = Math.floor(Math.random()*400) + 1000;
			
			outString = outString + (
				"(" + groupCounter + ", " + 
				randStud + ", " +
				gibGen() + "),<br>" //using gibGen
			);
			
			//a global leaderArray for use elsewhere
			leaderArray[groupCounter] = randStud
			
			counter++;
		}

		groupCounter++;
	} 

	document.getElementById("outGroupStudentLinkFunc").innerHTML = (
		outString	
	);

}

function outProjectFunc(){
	outString = "";
	projId = 1;

	while (projId <= 100){
		//random category id
		randCat = Math.floor(Math.random()*5)+1;

		//random presentation type
		randPresType = Math.floor(Math.random()*3)+1;

		if (projId <= 25){
			year = 2016;
		} else if (projId <= 50){
			year = 2017;
		} else if (projId <= 75){
			year = 2018;
		} else {
			year = 2019;
		}

		outString = outString + (
			"(" + projId + ", " +
			gibGen() + ", " + //for poster
			gibGen() + ", " + //for title
			gibGen() + ", " + //for abstract
			randPresType + ", " +
			year + ", " +
			projId + "),<br>"
		);

		projId++;
	}

	document.getElementById("outProject").innerHTML = (
		outString	
	);

}

function outMentorFunc(){
	outString = "";
	mentorId = 1;

	while (mentorId <= 50){
		randNumF = Math.floor(Math.random()*22);
		randNumL = Math.floor(Math.random()*22);

		//only 5 majors
		randMajorId = Math.floor(Math.random()*5)+1;

		outString = outString + (
			"(" + mentorId + ", '" +
			fName[randNumF] + "', '" +
			lName[randNumL] + "', '" +
			fName[randNumF] + mentorId + "@uscb.edu', " +
			randMajorId +
			"),<br>"
		);

		mentorId++;
	};
	document.getElementById("outMentor").innerHTML = (
		outString	
		
	);

}

function outFunc(){
	outNameFunc();
	//gibGen();
	//outGroupFunc();
	outGroupStudentLinkFunc(); //this creates an array which
				   //must be generated
				   //before outGroupFunc
	outGroupFunc();
	outProjectFunc();

	outMentorFunc(); //this must be called after outNameFunc()
}
