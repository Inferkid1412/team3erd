--1 What was student with ID 1234 name and what group(s) did they work in if any?
--CREATE VIEW Q1
--AS
SELECT
	Student.StudentID,
	StudentFName,
	StudentLName,
	GroupID
FROM
	Student INNER JOIN GroupStudentLink
	ON Student.StudentID = GroupStudentLink.StudentID
WHERE
	Student.StudentID = 1234

--2 Who was the speaker in 2017 and what was their description?
SELECT
	SpeakerFName,
	SpeakerLName,
	Descriptions
FROM
	Speaker
WHERE
	Year = 2017

--3 Who all made project 66 and what was project 66s title?
SELECT
	Project.ProjectID,
	Title,
	StudentFName
FROM
	Project INNER JOIN OurGroups
	ON Project.GroupID = OurGroups.GroupID
	INNER JOIN GroupStudentLink
	ON OurGroups.GroupID = GroupStudentLink.GroupID
	INNER JOIN Student
	ON GroupStudentLink.StudentID = Student.StudentID
WHERE Project.ProjectID = 66

--4 What roles has the leader of group 21 had and in which groups?
SELECT
	GroupStudentLink.GroupID,
	GroupLeaderID,
	GroupStudentLink.Roles
FROM
	OurGroups RIGHT JOIN GroupStudentLink
	ON OurGroups.GroupLeaderID = GroupStudentLink.StudentID
WHERE OurGroups.GroupID = 21

--5 In what year was project 92 made?
SELECT
	Year
FROM
	Project INNER JOIN OurGroups
	ON Project.GroupID = OurGroups.GroupID
WHERE Project.ProjectID = 92

--6 What were the names of the students who were in groups in 2017?
SELECT
	Student.StudentID,
	StudentFName,
	StudentLName
FROM
	OurGroups INNER JOIN GroupStudentLink
	ON OurGroups.GroupID = GroupStudentLink.GroupID
	INNER JOIN Student
	ON GroupStudentLink.StudentID = Student.StudentID
WHERE Year = 2017

--7 What were the categories of the awards given in 2017?
SELECT
	AwardTitle,
	CategoryName
FROM
	Awards INNER JOIN Categories
	ON Awards.CategoryID = Categories.CategoryID
WHERE Year = 2017

--8 When were the registration deadlines each year?
SELECT
	AcademicYear,
	RegistrationDeadline
FROM
	Registration

--9 When were the final draft deadlines each year?
SELECT
	AcademicYear,
	FinalDraftDeadline
FROM
	Registration

--10 When was the mentor approval deadline in 2018?
SELECT
	AcademicYear,
	MentorApprovalDeadline
FROM
	Registration
WHERE
	AcademicYear = 2018

--11 Which projects were presented with video?
SELECT
	ProjectID,
	Title,
	PresentationTypeID
FROM
	Project
WHERE
	PresentationTypeID = 2

--12 What were the roles of the students who were in groups in 2019?
SELECT
	Roles
FROM
	OurGroups INNER JOIN GroupStudentLink
	ON OurGroups.GroupID = GroupStudentLink.GroupID
WHERE
	Year = 2019

--13 When were the events held?
SELECT
	EventYear,
	EventDate
FROM
	Event

--14 Where were the events held?
SELECT
	EventYear,
	EventLocation
FROM
	Event

--15 What were the event schedules?
SELECT
	EventYear,
	EventSchedule
FROM
	Event

--16 Who are the mentors and what are their fields of expertece?
SELECT
	MentorID,
	MentorFName,
	MentorLName,
	MajorID
FROM
	Mentor

--17 Who are the sponsors and what are the awards that they gave?
SELECT
	SponsorName,
	AwardTitle
FROM
	Sponsors INNER JOIN Awards
	ON Sponsors.SponsorID = Awards.SponsorID

--18 Who are the judges?
SELECT
	JudgeFName,
	JudgeLName
FROM
	Judges

--19 Are there any students named "Moon Moon"? :3
SELECT
	StudentFName,
	StudentLName
FROM
	Student
WHERE StudentFName = 'Moon' AND StudentLName = 'Moon'

--20 Are there any students named "Sam Hill" and what is their email? ;)
SELECT
	StudentFName,
	StudentLName,
	StudentEmail
FROM
	Student
WHERE StudentFName = 'Sam' AND StudentLName = 'Hill'
--OwO

--21 Who all are in the Hill family and what are their emails?
SELECT
	StudentFName,
	StudentLName,
	StudentEmail
FROM
	Student
WHERE StudentLName = 'Hill'

--22 So many William Hills! In which groups were there William Hills?
SELECT
	GroupID,
	StudentFName,
	StudentLName
FROM
	Student JOIN GroupStudentLink
	ON Student.StudentID = GroupStudentLink.StudentID
WHERE StudentFName = 'William' AND StudentLName = 'Hill'
ORDER BY GroupID

--23 How many mentors have the first name of 'Sam' and what are their majors?
