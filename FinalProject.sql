﻿-- SQL server code for B320 SRSD ProjectID
-- Dev Status: Active (not complete)

/* Is this table necisary? In Project a column is named Poster*/
/*
CREATE TABLE [GroupSubmission] (
  [GroupID] int,
  [Poster] varchar(max), 
  PRIMARY KEY ([GroupID])
);
*/

CREATE TABLE [Speaker] (
  [SpeakerID] int,
  [Year] int,
  [SpeakerFName] varchar(max),
  [SpeakerLName] varchar(max),
  [Descriptions] varchar(max),
  PRIMARY KEY ([SpeakerID])
); --Done

CREATE TABLE [Judges] (
  [JudgeID] int,
  [Year] int,
  [JudgeFName] varchar(max),
  [JudgeLName] varchar(max),
  [JudgeEmail] varchar(max),
  PRIMARY KEY ([JudgeID])
); --Done

CREATE TABLE [Awards] (
  [ProjectID] int,
  [SponsorId] int,
  [AwardTitle] varchar(max),
  [AwardAmount] int,
  [CategoryID] int,
  [Year] int,
  PRIMARY KEY ([ProjectID])
); --Done

CREATE TABLE [OurGroups] (
  [GroupID] int,
  [Year] int, /*When this group was active*/
  [MentorID] int,
  [CategoryID] int,
  [GroupLeaderID] int,
  [ProjectID] int,
  PRIMARY KEY ([GroupID])
); --Done

/* linking table to link students to groups */
CREATE TABLE [GroupStudentLink] (
	[GroupID] int,
	[StudentID] int,
	[Roles] varchar(max)
); --Done

/*not connected to rest of the table, still important */
CREATE TABLE [Registration] (
  [AcademicYear] int,
  [FinalDraftDeadline] varchar(max),
  [MentorApprovalDeadline] varchar(max),
  [AbstractDeadline] varchar(max),
  [RegistrationDeadline] varchar(max),
  PRIMARY KEY ([AcademicYear])
); --Done

/* Not connected to rest of the table, still important */
CREATE TABLE [Event] (
  [EventDate] varchar(max),
  [EventLocation] varchar(max),
  [EventSchedule] varchar(max),
  [EventYear] int,
  PRIMARY KEY ([EventYear])
); --Done

/* linked to MentorID, PrimaryStudentID, GroupID, PresentationTypes*/
CREATE TABLE [Project] (
  [ProjectID] int,
  [Poster] varchar(max),
  [Title] varchar(max),
  /*[CategoryID] int,
  [MentorID] int,
  //removed these as group contains categoryId and mentorId
  */
  [Abstract] varchar(max),
  [PresentationTypeID] int,
  [AcademicYear] int,
  /*[PrimaryStudentID] int, group leader in group table is primary student*/
  [GroupID] int,
  PRIMARY KEY ([ProjectID])
); --Done

/* table added to insure 3rd normilization */
CREATE TABLE [Categories] (
	[CategoryID] int,
	[CategoryName] varchar(max)
); --Done

/* Created to insure 3rd normalization */
CREATE TABLE [PresentationTypes] (
	[PresentationTypeID] int,
	[PresentationType] varchar(max)
	PRIMARY KEY ([PresentationTypeID])
); --Done

/* For our purposes every student is in a group and their MentorID, ProjectID, JudgeID is determined by what group they are in */
CREATE TABLE [Student] (
  [StudentID] int,
  [StudentFName] varchar(max),
  [StudentLName] varchar(max),
  [StudentEmail] varchar(max),
  [MajorID] int,
  PRIMARY KEY ([StudentID])
); --Done

/* Created this table to insure third normalization */
CREATE TABLE [Majors] (
	[MajorID] int,
	[MajorName] varchar(max),
	PRIMARY KEY ([MajorID])
); --Done

CREATE TABLE [Sponsors] (
  [SponsorID] int,
  [Year] int,
  [SponsorName] varchar(max),
  [SponsorAmount] int,
  PRIMARY KEY ([SponsorID])
); --Done

CREATE TABLE [Mentor] (
  [MentorID] int,
  [MentorFName] varchar(max),
  [MentorLName] varchar(max),
  [MentorEmail] varchar(max),
  [MajorID] int, /*What this mentor teaches*/
  PRIMARY KEY ([MentorID])
); --Done

/* what is this?
INSERT GroupSubmission (GroupID,PosterName) VALUES
(1,Infernape),

INSERT Speaker (SpeakerID,SpeakerFName,SpeakerLName,Descriptions) VALUES
(1,Infernape)
*/

/* This here is for testing purposes getting rid of all tables
DROP TABLE [Mentor];
DROP TABLE [Sponsors];
DROP TABLE [Student];
DROP TABLE [Project];
DROP TABLE [Event];
DROP TABLE [Registration];
DROP TABLE [OurGroups];
DROP TABLE [Awards];
DROP TABLE [Judges];
DROP TABLE [Speaker];
DROP TABLE [GroupStudentLink];
DROP TABLE [PresentationTypes];
DROP TABLE [Majors];
DROP TABLE [Categories];
*/
